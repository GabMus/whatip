from gi.repository import Gio
from whatip.conf_keys import ConfKeys
from whatip.base_app import BaseApp, BaseWindow, AppShortcut
from whatip.main_ui import MainUI


class AppWindow(BaseWindow):
    def __init__(self, app: BaseApp):
        super().__init__(
            app=app,
            app_name='What IP',
            icon_name='org.gabmus.whatip',
            shortcuts=[AppShortcut(
                'F10', lambda *_: self.main_ui.menu_btn.popup()
            )]
        )

        self.main_ui = MainUI(self.app.gsettings)
        self.append(self.main_ui)

        self.dark_mode = bool(
            self.app.gsettings.get_boolean(ConfKeys.DARK_MODE)
        )
        self.app.gsettings.bind(ConfKeys.DARK_MODE, self,
                                'dark_mode', Gio.SettingsBindFlags.DEFAULT)

    def present(self):
        super().present()
        w, h = (
            int(self.app.gsettings.get_value(key).get_int32())
            for key in (ConfKeys.WIN_WIDTH, ConfKeys.WIN_HEIGHT)
        )
        self.set_default_size(w, h)

    def emit_destroy(self, *_):
        self.emit('close-request')

    def on_destroy(self, *_):
        self.app.gsettings.set_int(ConfKeys.WIN_WIDTH, self.get_width())
        self.app.gsettings.set_int(ConfKeys.WIN_HEIGHT, self.get_height())

    def do_startup(self):
        self.main_ui.network_iterface_store.populate()
        self.main_ui.port_store.populate()
