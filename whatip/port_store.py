from gi.repository import Gtk, Gio
from whatip.network_ports import NetPort, get_ports
from whatip.port_listbox_row import PortListboxRow


class PortStore(Gtk.SortListModel):
    def __init__(self, listbox: Gtk.ListBox):
        self.listbox = listbox

        self.sorter = Gtk.CustomSorter()
        self.sorter.set_sort_func(self.__sort_func)
        self.list_store = Gio.ListStore(item_type=NetPort)
        super().__init__(model=self.list_store)

        self.listbox.bind_model(self, self.__create_listbox_row, None)

    def __create_listbox_row(
        self, item: NetPort, *_
    ) -> Gtk.ListBoxRow:
        row = PortListboxRow(item)
        return row

    def __sort_func(self, p1: NetPort, p2: NetPort, *_):
        if p1.port_num < p2.port_num:  # type: ignore
            return -1
        return 1

    def populate(self):
        self.list_store.remove_all()
        for port in get_ports():
            self.list_store.append(port)
