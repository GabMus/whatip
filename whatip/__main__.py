# __main__.py
#
# Copyright (C) 2019 GabMus
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys

# import argparse
# from os.path import isfile
from gettext import gettext as _
from typing import cast
from gi.repository import Gtk, Gio, Adw
from whatip.app_window import AppWindow
from whatip.conf_keys import ConfKeys
from whatip.preferences_window import PreferencesWindow
from whatip.base_app import BaseApp, AppAction


class GApplication(BaseApp):
    def __init__(self):
        super().__init__(
            app_id='org.gabmus.whatip',
            app_name='What IP',
            app_actions=[
                AppAction(
                    name='preferences',
                    func=self.show_preferences_window,
                    accel='<Primary>comma',
                ),
                AppAction(
                    name='shortcuts',
                    func=self.show_shortcuts_window,
                    accel='<Primary>question',
                ),
                AppAction(name='about', func=self.show_about_dialog),
                AppAction(
                    name='quit',
                    func=self.on_destroy_window,
                    accel='<Primary>q',
                ),
            ],
            flags=Gio.ApplicationFlags.DEFAULT_FLAGS,
            css_resource='/org/gabmus/whatip/ui/gtk_style.css',
        )

    def show_about_dialog(self, *args):
        dialog = cast(Adw.AboutWindow, Gtk.Builder.new_from_resource(
            '/org/gabmus/whatip/aboutdialog.ui'
        ).get_object('aboutdialog'))
        dialog.set_modal(True)
        dialog.set_transient_for(self.window)
        dialog.present()

    def on_destroy_window(self, *args):
        self.window.on_destroy()
        self.quit()

    def show_shortcuts_window(self, *args):
        shortcuts_win = cast(
            Gtk.ShortcutsWindow, Gtk.Builder.new_from_resource(
                '/org/gabmus/whatip/ui/shortcutsWindow.ui'
            ).get_object('shortcuts-win'))
        shortcuts_win.props.section_name = 'shortcuts'
        shortcuts_win.set_transient_for(self.window)
        shortcuts_win.set_modal(True)
        shortcuts_win.present()
        shortcuts_win.show()

    def show_preferences_window(self, *args):
        preferences_win = PreferencesWindow(self.window)
        preferences_win.present()

    def do_activate(self):
        super().do_activate()
        self.window = AppWindow(self)
        self.window.connect('close-request', self.on_destroy_window)
        self.add_window(self.window)
        self.window.present()
        # self.window.show()

        if bool(self.gsettings.get_boolean(ConfKeys.FIRST_LAUNCH)):
            disclaimer_dialog = Gtk.MessageDialog(
                message_type=Gtk.MessageType.QUESTION,
                buttons=Gtk.ButtonsType.YES_NO,
                text=_(
                    'What IP connects to third party services to retrieve '
                    'geolocation information, your public IP and determine if '
                    'you are online.\n\n'
                    'Do you want to keep this feature enabled?\n'
                    'You can always change this setting in the application '
                    'preferences.'
                ),
                transient_for=self.window,
                modal=True,
            )
            try:
                child = disclaimer_dialog.get_first_child()
                while not isinstance(child, Gtk.Label) and child is not None:
                    child = child.get_first_child()
                if child is not None:
                    child.set_max_width_chars(30)
                    child.set_vexpand(True)
            except Exception:
                pass

            def on_response(dialog, response):
                disclaimer_dialog.close()
                self.gsettings.set_boolean(ConfKeys.FIRST_LAUNCH, False)
                self.gsettings.set_boolean(
                    ConfKeys.ENABLE_THIRD_PARTIES,
                    response == Gtk.ResponseType.YES,
                )
                self.continue_activate()

            disclaimer_dialog.set_transient_for(self.window)
            disclaimer_dialog.connect('response', on_response)
            disclaimer_dialog.present()
        else:
            self.continue_activate()

    def continue_activate(self):
        self.window.do_startup()


def main():
    application = GApplication()

    try:
        ret = application.run(sys.argv)
    except SystemExit as e:
        ret = e.code

    sys.exit(ret)


if __name__ == '__main__':
    main()
