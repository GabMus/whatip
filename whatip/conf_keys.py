class ConfKeys:
    WIN_WIDTH = 'window-width'
    WIN_HEIGHT = 'window-height'
    DARK_MODE = 'dark-mode'
    ENABLE_THIRD_PARTIES = 'enable-third-parties'
    FIRST_LAUNCH = 'first-launch'
