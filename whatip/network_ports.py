from gi.repository import GObject
from enum import Enum
from subprocess import check_output
from typing import Iterable, List, Optional, Set


class PortType(Enum):
    TCP = 'TCP'
    UDP = 'UDP'


class NetPortProcess:
    def __init__(self, name: str, pid: int):
        self.name = name
        self.pid = pid

    @classmethod
    def from_ss_section(cls, txt: str) -> Optional['NetPortProcess']:
        try:
            sp = txt.split(':')[-1].strip('((').strip('))').split(',')
            name = sp[0].strip('"').strip()
            pid = int(sp[1].strip('pid=').strip())
            return cls(name, pid)
        except Exception:
            return None

    def __eq__(self, other: 'NetPortProcess'):
        return self.name == other.name and self.pid == other.pid

    def __lt__(self, other: 'NetPortProcess'):
        return self.pid < other.pid


class NetPort(GObject.Object):
    __port_num: int
    __addresses: Iterable[str]
    __types: Iterable[PortType]
    __process: Optional[NetPortProcess]

    def __init__(
        self,
        port_num: int,
        addresses: Set[str],
        types: Set[PortType],
        process: Optional[NetPortProcess] = None,
    ):
        super().__init__()
        self.__port_num = port_num
        self.__addresses = addresses
        self.__types = types
        self.__process = process

    @GObject.Property(type=int)
    def port_num(self) -> int:
        return self.__port_num

    @GObject.Property
    def addresses(self) -> Iterable[str]:
        return self.__addresses

    @GObject.Property
    def types(self) -> Iterable[PortType]:
        return self.__types

    @GObject.Property
    def process(self) -> Optional[NetPortProcess]:
        return self.__process

    def __eq__(self, other: 'NetPort'):
        return (
            self.port_num == other.port_num
            and self.addresses == other.addresses
            and self.types == other.types
            and self.process == other.process
        )

    def __lt__(self, other: 'NetPort'):
        return self.port_num < other.port_num  # type: ignore


def as_port_type(typ: str) -> PortType:
    res = {'tcp': PortType.TCP, 'udp': PortType.UDP}.get(typ, None)
    if res is None:
        raise ValueError(f'Port type `{typ}` not recognized')
    return res


def get_ports() -> List[NetPort]:
    ports: List[NetPort] = list()
    out = check_output(['ss', '-tulpnH']).decode()
    for line in out.split('\n'):
        if not line:
            continue
        line_split = line.split()
        typ = as_port_type(line_split[0])
        addr_num_sect = line_split[4]
        num = addr_num_sect.split(':')[-1]
        address = addr_num_sect[:addr_num_sect.index(num)-1]
        process = (
            NetPortProcess.from_ss_section(line_split[-1])
            if len(line_split) == 7
            else None
        )
        existing_port_obj = next(
            (port_obj for port_obj in ports if port_obj.port_num == int(num)),
            None,
        )
        if existing_port_obj is None:
            ports.append(NetPort(int(num), {address}, {typ}, process))
        else:
            existing_port_obj.addresses.add(address)
            existing_port_obj.types.add(typ)
    return sorted(ports)
