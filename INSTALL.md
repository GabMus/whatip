## Installing from Flathub

You can install What IP via [Flatpak](https://flathub.org/apps/details/org.gabmus.whatip).

## Installing from AUR

What IP is available as an AUR package: [`whatip-git`](https://aur.archlinux.org/packages/whatip-git/).

## Building on Ubuntu/Debian

```bash
sudo apt-get install python-requests

git clone https://gitlab.gnome.org/World/whatip
cd whatip
mkdir build
cd build
meson ..
meson configure -Dprefix=$PWD/testdir # use this line if you want to avoid installing system wide
ninja
ninja install
```

## Building on Arch/Manjaro

```bash
sudo pacman -S python-requests python-gobject

git clone https://gitlab.gnome.org/GabMus/whatip
cd whatip
mkdir build
cd build
meson ..
meson configure -Dprefix=$PWD/testdir # use this line if you want to avoid installing system wide
ninja
ninja install
```

## Hacking

You might want to check your code with pyflakes before you commit.

- Ubuntu/Debian: `sudo apt install pyflakes`
- Arch/Manjaro: `sudo pacman -S python-pyflakes`

```bash
pyflakes whatip
```
