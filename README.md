# <a href="https://gabmus.gitlab.io/whatip"><img height="32" src="data/icons/org.gabmus.whatip.svg" /> What IP</a>

Info on your IP

![screenshot](https://gitlab.gnome.org/GabMus/whatip/-/raw/website/static/screenshots/mainwindow.png)

## Notes on the distribution of this app

I decided to target flatpak mainly. It's just another package manager at the end of the day, but
it's supported by many Linux distributions. It bundles all of the dependencies you need in one
package.

This helps a lot in supporting many different distros because I know which version of which
dependency you have installed, so I don't have to mess with issues caused by version mismatches.
If you want to report an issue, feel free to. But please at least first see if this issue happens
with the flatpak version as well.

As for development it's a similar story. I do most of my testing directly inside a flatpak sandbox
and you should do the same. It's easy to set up, just open up this repo in GNOME Builder and press
the run button. It will handle the rest.

## Installing from Flathub

You can install What IP via [Flatpak](https://flathub.org/apps/details/org.gabmus.whatip).

## Installing from AUR

What IP is available as an AUR package: [`whatip-git`](https://aur.archlinux.org/packages/whatip-git/).

<!--
## Installing from Fedora

What IP is available in [Fedora repos](https://apps.fedoraproject.org/packages/whatip): `sudo dnf install whatip`
-->

# Building on different distributions

**Note**: these are illustrative instructions. If you're a developer or a package maintainer, they
can be useful to you. If not, just install the flatpak.

## Building on Ubuntu/Debian

```bash
sudo apt-get install python-requests

git clone https://gitlab.gnome.org/GabMus/whatip
cd whatip
mkdir build
cd build
meson ..
meson configure -Dprefix=$PWD/testdir # use this line if you want to avoid installing system wide
ninja
ninja install
```

## Building on Arch/Manjaro

```bash
sudo pacman -S python-requests python-gobject

git clone https://gitlab.gnome.org/GabMus/whatip
cd whatip
mkdir build
cd build
meson ..
meson configure -Dprefix=$PWD/testdir # use this line if you want to avoid installing system wide
ninja
ninja install
```
